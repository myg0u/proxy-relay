package main

import (
    "encoding/json"
    "flag"
    "fmt"
    "net/http"
    "proxy-relay/internal/proxy2808"
)

func main() {
    token := flag.String("token", "", "2808 proxy token")
    expire := flag.Int("expire", 60, "expiration seconds")
    listen := flag.String("listen", "0.0.0.0:2323", "listen address")
    flag.Parse()



    c := proxy2808.Client{Token: *token, Expire: *expire}
    cacher := proxy2808.NewCacher()
    cacher.AddClient(&c)


    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request){
    	fmt.Fprintf(w, "Proxy Cacher Service, Welcome!")
    })

    http.HandleFunc("/proxy", func(w http.ResponseWriter, r *http.Request){
        w.Header().Set("Content-Type", "application/json")
        err := json.NewEncoder(w).Encode(cacher.FetchSingle())
        if err != nil {
            http.Error(w, err.Error(), 500)
        }
    })

    http.ListenAndServe(*listen, nil)
}

