# proxy-relay

a common proxy cacher for frequently fetch proxy.


# Usage

## Startup

### For MAC OS X 

bin/proxy2808-relay-macos-0.0.1 -token YOUR-TOKEN -listen 0.0.0.0:2323 -expire 60

### For Linux

bin/proxy2808-relay-linux-amd64-0.0.1 -token YOUR-TOKEN -listen 0.0.0.0:2323 -expire 60


## Access

curl http://localhost:2323/proxy
