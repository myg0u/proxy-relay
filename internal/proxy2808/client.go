package proxy2808

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)


type Client struct {
	Token string
	Expire int
}

type Interface struct {
	Id                string    `json:"id"`
	Ip                string    `json:"ip"`
	HttpPort          int       `json:"http_port"`
	ExpireAtTimestamp float32   `json:"expire_at_timestamp"`
	Client 			  *Client `json:"-"`
}

type Response struct {
	Status int `json:"status"`
	Msg string `json:"msg"`
	Data []Interface
}

func (r *Response) isOK () bool {
	return r.Status == 0
}

func (c *Client) GetProxies(amount int) ([]Interface, error) {
	resp, err := c.GetUrl(fmt.Sprintf("proxy/unify/get?token=%s&amount=%d&proxy_type=http", c.Token, amount))
	if err != nil {
		return nil, err
	}
	if resp.isOK() {
		var proxies []Interface
		for _, i := range resp.Data {
			i.Client = c
			proxies = append(proxies, i)
		}
		return proxies, nil
	} else {
		return nil, errors.New(resp.Msg)
	}
}

func (c *Client) ReleaseProxy(id string) error {
	_, err := c.GetUrl(fmt.Sprintf("proxy/release?token=%s&id=%s",c.Token,id))
	return err
}

func (c *Client) GetUrl(uri string) (*Response, error) {
	var response Response
	host :=  "http://api.2808proxy.com/"
	addr := host +uri
	log.Printf("Requested %s \n", addr)
	resp , err := http.Get(addr)
	if err != nil {
		log.Printf(err.Error())
		return nil, err
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	log.Printf("Resp: %s\n",body)

	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, err
	}
	return &response, nil
}

func (c *Client) isAlive() bool {
	resp, err := c.GetUrl("api/profile?token="+c.Token)
	if err != nil {
		return false
	} else {
		return resp.Status == 0
	}
}
