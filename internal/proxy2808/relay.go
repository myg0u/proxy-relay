package proxy2808

import (
	"log"
	"math/rand"
	"sync"
	"time"
)

type Cacher struct {
	locker  sync.RWMutex
	cacher  map[string]Interface
	clients []*Client
}

func NewCacher() *Cacher {
	cacher := Cacher{
		cacher: make(map[string]Interface, 64),
	}
	cacher.run()
	return &cacher
}

func (c *Cacher) addProxy(key string, proxy Interface) {
	c.locker.Lock()
	c.cacher[key] = proxy
	c.locker.Unlock()
}
func (c *Cacher) deleteProxy(key string) {
	c.locker.Lock()
	delete(c.cacher,key)
	c.locker.Unlock()
}

func (c *Cacher) FetchSingle() *Interface {
	c.locker.RLock()
	var keys []string
	for k := range c.cacher {
		keys = append(keys, k)
	}
	c.locker.RUnlock()
	if len(keys) < 1 {
		return nil
	}
	index := rand.Intn(len(keys))
	if index >= 0 && len(keys) > 0 {
		key := keys[index]
		i := c.cacher[key]
		return &i
	} else {
		log.Printf("Response nil because empty list.\n")
		return nil
	}
}

func (c *Cacher) AddClient(cli *Client) {
	c.locker.Lock()
	c.clients = append(c.clients, cli)
	c.locker.Unlock()
}

func (c *Cacher) RefreshAllClientProxies() error {
	var e error = nil
	for _, cli := range c.clients {
		err :=c.refreshProxies(cli)
		if err != nil{
			e = err
		}
	}
	return e
}

func (c *Cacher) EvictProxies(){
	for key, val:= range c.cacher {
		now := time.Now().Unix()
		expire := int64(val.ExpireAtTimestamp)
		if expire < now {
			log.Printf("Interface %s expire at %d, now is %d, expire it.", val.Id, expire, now)
			err := val.Client.ReleaseProxy(val.Id)
			c.deleteProxy(key)
			if err != nil {
				log.Printf("[warn] when release interface:%s", err)
			}
		}
	}
}


func (c *Cacher) refreshProxies(cli *Client) error {
	proxies, err := cli.GetProxies(30)
	if err != nil {
		log.Printf("[warn] when refresh proxies, err:%s\n", err.Error())
		return err
	}
	for _, v := range proxies {
		log.Printf("[info] get proxy info to cacher, id : %s, ip:%s\n", v.Id, v.Ip)
		c.addProxy(v.Id, v)
	}
	return nil
}

func (c *Cacher) run() {
	go func() {
		for {
			log.Printf("cacher (requests/size) (%d,!!)", len(c.cacher))
			err := c.RefreshAllClientProxies()
			if err != nil {
				log.Printf("err when refresh all proxies:%s\n", err.Error())
			}
			c.EvictProxies()
			time.Sleep(10 * time.Second)
		}
	}()
}
