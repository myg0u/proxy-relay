#!/usr/bin/env bash

dirname $0
export GOOS=linux
export GOARCH=amd64
echo $GOPATH

cd `dirname $0`

go build -a -i -o ./bin/proxy2808-relay-linux-amd64-0.0.1 cmd/proxy2808-relay/main.go


